from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.views.generic import CreateView


class SignupView(CreateView):
    template_name = "registration/signup.html"
    success_url = reverse_lazy("login")
    form_class = UserCreationForm

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.purchaser = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("home", pk=plan.id)
