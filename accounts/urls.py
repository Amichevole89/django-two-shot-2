from django.urls import path
from django.contrib.auth.views import LogoutView, LoginView
from accounts.views import SignupView
from django.contrib import auth

urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("signup/", SignupView.as_view(), name="signup"),
]
