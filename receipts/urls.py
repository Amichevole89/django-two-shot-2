from django.urls import path
from receipts.views import ReceiptListView, ReceiptCreateView
from receipts.views import AccountListView, AccountCreateView
from receipts.views import ExpenseCategoryListView, ExpenseCategoryCreateView

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path(
        "categories/",
        ExpenseCategoryListView.as_view(),
        name="list_categories",
    ),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="create_category",
    ),
    path("accounts/", AccountListView.as_view(), name="list_account"),
    path(
        "accounts/create/", AccountCreateView.as_view(), name="create_account"
    ),
]
