from django.shortcuts import render, redirect
from django.views.generic import ListView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from receipts.models import Receipt, Account, ExpenseCategory
from django.urls import reverse_lazy


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipt/list.html"
    # context_object_name = "receiptlist"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipt/create.html"
    success_url = reverse_lazy("create_receipt")
    fields = [
        "vendor",
        "total",
        "tax",
        "date",
        "category",
        "account",
    ]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect("home")

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "account/list.html"
    fields = ["name", "number"]
    # context_object_name = "accountlist"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "account/create.html"
    fields = ["name", "number"]
    success_url = reverse_lazy("list_account")

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("list_account")


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "category/list.html"
    fields = ["name"]
    # context_object_name = "expenselist"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "category/create.html"
    fields = ["name"]
    success_url = reverse_lazy("list_categories")

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("list_categories")
