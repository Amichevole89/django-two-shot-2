from django.contrib import admin
from receipts.models import Account, Receipt, ExpenseCategory

admin.site.register(ExpenseCategory),
admin.site.register(Account),
admin.site.register(Receipt)
